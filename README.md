# Sidebar App
[![Netlify Status](https://api.netlify.com/api/v1/badges/c513dd8b-a682-45f4-9a8c-eef409de4800/deploy-status)](https://app.netlify.com/sites/my-react-testing-at/deploys)
Learning react.

## Built With
* [React](https://reactjs.org/) - The framework used
* [Netlify](https://www.netlify.com/) - builds, deploys and hosts your front-end.

## Authors
> **Janmejay S Purohit** - *Initial work*

## License
This project is a part of [Tesark](https://www.tesark.com/).