import React from "react";
import DatePicker from "react-datepicker";

function RightPane(props) {
  return (
    <div>
      <h1 className="font-bold text-center text-4xl">Preview</h1>
      <div className="flex flex-wrap">
        <div className="w-1/5"></div>
        <div className="w-3/5">
          <table className="table-fixed w-full text-center text-gray-700">
            <thead>
              <tr>
                <th className="w-1/3 px-4 py-2">Property</th>
                <th className="w-2/3 px-4 py-2">Preview</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td className="border px-4 py-2">Name</td>
                <td className="border px-4 py-2">{props.fdata.data.name}</td>
              </tr>
              <tr className="bg-gray-100">
                <td className="border px-4 py-2">E-Mail</td>
                <td className="border px-4 py-2">{props.fdata.data.email}</td>
              </tr>
              <tr>
                <td className="border px-4 py-2">Password</td>
                <td className="border px-4 py-2">
                  {props.fdata.data.password}
                </td>
              </tr>
              <tr className="bg-gray-100">
                <td className="border px-4 py-2">Bio</td>
                <td className="border px-4 py-2 w-full">
                  {props.fdata.data.bio}
                </td>
              </tr>
              <tr>
                <td className="border px-4 py-2">Fruit</td>
                <td className="border px-4 py-2">
                  {props.fdata.data.fruits.map((fruitid) => {
                    return (
                      props.fdata.options.fruitsOptions
                        .filter((fruit) => fruit.id.toString() === fruitid)
                        .map((fr) => fr.value) + ", "
                    );
                  })}
                </td>
              </tr>
              <tr className="bg-gray-100">
                <td className="border px-4 py-2">Number Of Friends</td>
                <td className="border px-4 py-2">
                  {props.fdata.data.numberOfFriends}
                </td>
              </tr>
              <tr>
                <td className="border px-4 py-2">Gender</td>
                <td className="border px-4 py-2">{props.fdata.data.gender}</td>
              </tr>
              <tr className="bg-gray-100">
                <td className="border px-4 py-2">Multiselect</td>
                <td className="border px-4 py-2">
                  {props.fdata.data.multiSelect.join(", ")}
                </td>
              </tr>
              <tr>
                <td className="border px-4 py-2">Request Limit</td>
                <td className="border px-4 py-2">
                  {props.fdata.data.requestLimit}
                </td>
              </tr>
              <tr className="bg-gray-100">
                <td className="border px-4 py-2">Range</td>
                <td className="border px-4 py-2">{props.fdata.data.range}</td>
              </tr>
              <tr>
                <td className="border px-4 py-2">List of names</td>
                <td className="border px-4 py-2">
                  {props.fdata.data.names.map((nameid) => {
                    return (
                      props.fdata.options.namesOptions
                        .filter((name) => name.id.toString() === nameid)
                        .map((nm) => nm.value) + ", "
                    );
                  })}
                </td>
              </tr>
              <tr className="bg-gray-100">
                <td className="border px-4 py-2">Birthday</td>
                <td className="border px-4 py-2">
                  <DatePicker
                    className="text-center bg-gray-100 w-full"
                    selected={props.fdata.data.birthday}
                    onSelect={""}
                    disabled
                    dateFormat="dd/MM/yyyy"
                  />
                </td>
              </tr>
              <tr>
                <td className="border px-4 py-2">List of colors</td>
                <td className="border px-4 py-2">
                  {props.fdata.data.colorSelected.join(", ")}
                </td>
              </tr>
              <tr className="bg-gray-100">
                <td className="border px-4 py-2">List of files uploaded</td>
                <td className="border px-4 py-2">
                  <ol className="list-decimal">
                    {props.fdata.data.avatarFile.map((file) => {
                      return <li>{file}</li>;
                    })}
                  </ol>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="w-1/5"></div>
      </div>
    </div>
  );
}
export default RightPane;
