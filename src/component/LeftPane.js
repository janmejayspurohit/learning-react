import React from "react";
import FormProvider from "./FormProvider";
import DatePicker from "react-datepicker";
import Dropzone from "react-dropzone";
import Select from "react-select";

class LeftPane extends React.Component {
  static contextType = FormProvider;
  render() {
    const context = this.context;
    const { data: inputData } = this.context;
    const { options: dataOptions } = this.context;

    return (
      <React.Fragment>
        <div>
          <h1 className="font-bold text-center text-4xl">Input</h1>
          <form
            className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4"
            onSubmit={context.handleSubmit}
          >
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Name:
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              name="name"
              type="text"
              placeholder="Your Name"
              value={inputData.name}
              onChange={context.handleStringInputChange}
            />
            {context.validator.message(
              "name",
              inputData.name,
              "required|alpha_space",
              { className: "text-red-600" }
            )}
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              E-Mail:
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              name="email"
              type="email"
              placeholder="jan@example.com"
              value={inputData.email}
              onChange={context.handleStringInputChange}
            />
            {context.validator.message(
              "email",
              inputData.email,
              "required|email",
              { className: "text-red-600" }
            )}
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Password:
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              name="password"
              type="password"
              placeholder="******************"
              value={inputData.password}
              onChange={context.handleStringInputChange}
            />
            {context.validator.message(
              "password",
              inputData.password,
              "required",
              { className: "text-red-600" }
            )}
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Avatar:
            </label>
            <label className="block shadow text-center appearance-none border rounded mt-2  w-40 py-2 px-3 cursor-pointer">
              <Dropzone
                onDrop={(acceptedFiles) => {
                  context.handleAvatarFileChange(acceptedFiles);
                }}
              >
                {({ getRootProps, getInputProps }) => (
                  <section>
                    <div {...getRootProps()}>
                      <input {...getInputProps()} />
                      <p>Browse...</p>
                    </div>
                  </section>
                )}
              </Dropzone>
            </label>
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Selected:{" "}
              <ol className="list-decimal">
                {inputData.avatarFile.map((file) => {
                  return <li className="text-red-600">{file}</li>;
                })}
              </ol>
            </label>
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Bio:
            </label>
            <textarea
              className="form-textarea mt-1 shadow appearance-none border rounded w-full break-words py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              rows="3"
              name="bio"
              type="text"
              value={inputData.bio}
              onChange={context.handleStringInputChange}
              placeholder="Tell us your story..."
              onBlur={() => context.validator.showMessageFor("bio")}
            ></textarea>
            {context.validator.message(
              "bio",
              inputData.bio,
              "required|min:20|max:120",
              { className: "text-red-600" }
            )}
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Birthday:
            </label>
            <DatePicker
              className="text-center shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              selected={inputData.birthday}
              onChange={context.handleDateChange}
              name="date"
              dateFormat="dd/MM/yyyy"
            />
            {context.validator.message("date", inputData.birthday, "required", {
              className: "text-red-600",
            })}
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Fruit :
            </label>
            {dataOptions.fruitsOptions.map((fruit, index) => {
              const checked = inputData.fruits.includes(fruit.id.toString());
              return (
                <label className="block inline-flex items-center text-gray-700 mt-2  text-sm font-bold mb-2">
                  <input
                    name="fruits"
                    className="ml-2 mr-1 leading-tight"
                    id={fruit.value}
                    type="checkbox"
                    checked={checked}
                    value={fruit.id}
                    onChange={context.handleCheckboxChange}
                  />
                  {fruit.value}
                </label>
              );
            })}
            {context.validator.message(
              "fruits",
              dataOptions.fruitsOptions,
              "required",
              { className: "text-red-600" }
            )}
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Number of Friends:
            </label>
            <input
              name="numberOfFriends"
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              type="number"
              value={inputData.numberOfFriends}
              onChange={context.handleStringInputChange}
            />
            <br /> <br />
            <label className="text-gray-700 text-sm font-bold mb-2">
              Gender:
            </label>
            <label className="text-gray-700 ml-2 text-sm mb-2">
              Male
              <input
                name="gender"
                className="form-radio ml-2"
                value="Male"
                checked={inputData.gender === "Male"}
                onChange={context.handleStringInputChange}
                type="radio"
              />
            </label>
            <label className="text-gray-700 ml-2 text-sm mb-2">
              Female
              <input
                name="gender"
                className="form-radio ml-2"
                value="Female"
                checked={inputData.gender === "Female"}
                onChange={context.handleStringInputChange}
                type="radio"
              />
            </label>
            {context.validator.message("gender", inputData.gender, "required", {
              className: "text-red-600",
            })}
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Requested Limit:
            </label>
            <select
              name="requestLimit"
              value={inputData.requestLimit}
              onChange={context.handleStringInputChange}
              className="form-select mt-1 bg-white border rounded w-full py-2 px-3 text-gray-700 leading-tight"
            >
              {dataOptions.requestLimitOptions.map(
                (requestLimitOption, index) => {
                  return <option>{requestLimitOption.value}</option>;
                }
              )}
            </select>
            <br />
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Range (between 0 and 50):
            </label>
            <input
              type="range"
              className="ml-2 mt-2"
              id="R"
              name="range"
              min="0"
              max="50"
              value={inputData.range}
              onChange={context.handleStringInputChange}
            />
            <br />
            <br />
            <label className="block">
              <span className="block text-gray-700 text-sm font-bold mt-2 mb-2">
                Multiselect
              </span>
              <select
                name="multiSelect"
                value={inputData.multiSelect}
                onChange={context.handleMultiSelectChange}
                className="form-multiselect block bg-white border rounded w-full py-2 px-3 text-gray-700 leading-tight mt-1"
                multiple
              >
                {dataOptions.multiSelectOptions.map(
                  (multiSelectOption, index) => {
                    return <option>{multiSelectOption.value}</option>;
                  }
                )}
              </select>
            </label>
            <br />
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              List of names :
            </label>
            {dataOptions.namesOptions.map((name, index) => {
              const checked = inputData.names.includes(name.id.toString());
              return (
                <label className="block items-center text-gray-700 mt-2  text-sm font-bold mb-2">
                  <input
                    name="names"
                    className="ml-2 mr-1 leading-tight"
                    id={name.value}
                    type="checkbox"
                    checked={checked}
                    value={name.id}
                    onChange={context.handleCheckboxChange}
                  />
                  {name.value}
                </label>
              );
            })}
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Select a color :
            </label>
            <Select
              isMulti
              name="colors"
              options={dataOptions.colorOptions}
              className="basic-multi-select"
              classNamePrefix="select"
              onChange={context.handleColorChange}
            />
            <br />
            <br />
            <input
              type="submit"
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              value="Submit"
            />
          </form>
        </div>
      </React.Fragment>
    );
  }
}

export default LeftPane;
