import React from 'react'

function footer() {
	return(
		<footer className="bg-gray-200 text-center text-as p-3 fixed bottom-0 w-full">
			&copy; Copyright 2020
		</footer>
	)
}

export default footer;