import React from "react";
import DatePicker from "react-datepicker";
import FormProvider from "./FormProvider";

class RightPane2 extends React.Component {
  static contextType = FormProvider;
  render() {
    let { data: inputData } = this.context;
    let { options: dataOptions } = this.context;
    return (
      <React.Fragment>
        <div>
          <h1 className="font-bold text-center text-4xl">Preview</h1>
          <div className="flex flex-wrap">
            <div className="w-1/5"></div>
            <div className="w-3/5">
              <table className="table-fixed w-full text-center text-gray-700">
                <thead>
                  <tr>
                    <th className="w-1/3 px-4 py-2">Property</th>
                    <th className="w-2/3 px-4 py-2">Preview</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td className="border px-4 py-2">Name</td>
                    <td className="border px-4 py-2">{inputData.name}</td>
                  </tr>
                  <tr className="bg-gray-100">
                    <td className="border px-4 py-2">E-Mail</td>
                    <td className="border px-4 py-2">{inputData.email}</td>
                  </tr>
                  <tr>
                    <td className="border px-4 py-2">Password</td>
                    <td className="border px-4 py-2">{inputData.password}</td>
                  </tr>
                  <tr className="bg-gray-100">
                    <td className="border px-4 py-2">Bio</td>
                    <td className="border px-4 py-2 w-full">{inputData.bio}</td>
                  </tr>
                  <tr>
                    <td className="border px-4 py-2">Fruit</td>
                    <td className="border px-4 py-2">
                      {inputData.fruits.map((fruitid) => {
                        return (
                          dataOptions.fruitsOptions
                            .filter((fruit) => fruit.id.toString() === fruitid)
                            .map((fr) => fr.value) + ", "
                        );
                      })}
                    </td>
                  </tr>
                  <tr className="bg-gray-100">
                    <td className="border px-4 py-2">Number Of Friends</td>
                    <td className="border px-4 py-2">
                      {inputData.numberOfFriends}
                    </td>
                  </tr>
                  <tr>
                    <td className="border px-4 py-2">Gender</td>
                    <td className="border px-4 py-2">{inputData.gender}</td>
                  </tr>
                  <tr className="bg-gray-100">
                    <td className="border px-4 py-2">Multiselect</td>
                    <td className="border px-4 py-2">
                      {inputData.multiSelect.join(", ")}
                    </td>
                  </tr>
                  <tr>
                    <td className="border px-4 py-2">Request Limit</td>
                    <td className="border px-4 py-2">
                      {inputData.requestLimit}
                    </td>
                  </tr>
                  <tr className="bg-gray-100">
                    <td className="border px-4 py-2">Range</td>
                    <td className="border px-4 py-2">{inputData.range}</td>
                  </tr>
                  <tr>
                    <td className="border px-4 py-2">List of names</td>
                    <td className="border px-4 py-2">
                      {inputData.names.map((nameid) => {
                        return (
                          dataOptions.namesOptions
                            .filter((name) => name.id.toString() === nameid)
                            .map((nm) => nm.value) + ", "
                        );
                      })}
                    </td>
                  </tr>
                  <tr className="bg-gray-100">
                    <td className="border px-4 py-2">Birthday</td>
                    <td className="border px-4 py-2">
                      <DatePicker
                        className="text-center bg-gray-100 w-full"
                        selected={inputData.birthday}
                        onSelect={""}
                        disabled
                        dateFormat="dd/MM/yyyy"
                      />
                    </td>
                  </tr>
                  <tr>
                    <td className="border px-4 py-2">List of colors</td>
                    <td className="border px-4 py-2">
                      {inputData.colorSelected.join(", ")}
                    </td>
                  </tr>
                  <tr className="bg-gray-100">
                    <td className="border px-4 py-2">List of files uploaded</td>
                    <td className="border px-4 py-2">
                      <ol className="list-decimal">
                        {inputData.avatarFile.map((file) => {
                          return <li>{file}</li>;
                        })}
                      </ol>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="w-1/5"></div>
          </div>{" "}
        </div>
      </React.Fragment>
    );
  }
}

export default RightPane2;
