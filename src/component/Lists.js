import React, { useState } from "react";

function NumberList(props) {
  const [numbers, setNumbers] = useState(props.numbers);
  const listItems = numbers.map((number) => (
    <li key={number.toString()}> {number}</li>
  ));
  return (
    <div>
      <ul>{listItems}</ul>
      <button
        className=" bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded"
        onClick={() => setNumbers(numbers.map((number) => number + 1))}
      >
        Add
      </button>
    </div>
  );
}

export default NumberList;
