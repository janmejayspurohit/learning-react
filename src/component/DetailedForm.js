import React from "react";
import Select from "react-select";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Dropzone from "react-dropzone";
import SimpleReactValidator from "simple-react-validator";
import RightPane from "./RightPane.js";

class DetailedForm extends React.Component {
  constructor(props) {
    super(props);
    this.validator = new SimpleReactValidator({ autoForceUpdate: this });
    this.state = {
      data: {
        name: "",
        email: "",
        password: "",
        avatarFile: [],
        birthday: new Date(),
        bio: "",
        numberOfFriends: "",
        gender: "",
        requestLimit: "",
        range: "",
        colorSelected: [],
        multiSelect: [],
        fruits: [],
        names: [],
      },
      options: {
        colorOptions: [
          { value: "ocean", label: "Ocean", color: "#00B8D9" },
          { value: "blue", label: "Blue", color: "#0052CC" },
          { value: "purple", label: "Purple", color: "#5243AA" },
          { value: "red", label: "Red", color: "#FF5630" },
          { value: "orange", label: "Orange", color: "#FF8B00" },
          { value: "yellow", label: "Yellow", color: "#FFC400" },
          { value: "green", label: "Green", color: "#36B37E" },
          { value: "forest", label: "Forest", color: "#00875A" },
          { value: "slate", label: "Slate", color: "#253858" },
          { value: "silver", label: "Silver", color: "#666666" },
        ],
        multiSelectOptions: [
          { id: 1, value: "Option 1" },
          { id: 2, value: "Option 2" },
          { id: 3, value: "Option 3" },
          { id: 4, value: "Option 4" },
          { id: 5, value: "Option 5" },
          { id: 6, value: "Option 6" },
          { id: 7, value: "Option 7" },
          { id: 8, value: "Option 8" },
          { id: 9, value: "Option 9" },
          { id: 10, value: "Option 10" },
        ],
        requestLimitOptions: [
          { id: 1, value: "---Select a value---" },
          { id: 2, value: "$1000" },
          { id: 3, value: "$2000" },
          { id: 4, value: "$3000" },
          { id: 5, value: "$4000" },
          { id: 5, value: "$5000" },
        ],
        fruitsOptions: [
          { id: 1, value: "Apple" },
          { id: 2, value: "Banana" },
          { id: 3, value: "Cherry" },
          { id: 4, value: "Coconut" },
          { id: 5, value: "Grape" },
          { id: 6, value: "Lime" },
          { id: 7, value: "Mango" },
          { id: 8, value: "Orange" },
          { id: 9, value: "Pear" },
          { id: 10, value: "Pineapple " },
        ],
        namesOptions: [
          { id: 1, value: "Name1" },
          { id: 2, value: "Name2" },
          { id: 3, value: "Name3" },
          { id: 4, value: "Name4" },
          { id: 5, value: "Name5" },
          { id: 6, value: "Name6" },
          { id: 7, value: "Name7" },
          { id: 8, value: "Name8" },
          { id: 9, value: "Name9" },
          { id: 10, value: "Name10" },
        ],
      },
    };
    this.handleStringInputChange = this.handleStringInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
    this.handleMultiSelectChange = this.handleMultiSelectChange.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handleAvatarFileChange = this.handleAvatarFileChange.bind(this);
  }

  SetDataCopy(dataCopy) {
    this.setState({
      data: dataCopy,
    });
  }

  handleStringInputChange(event) {
    let dataCopy = this.state.data;
    dataCopy[event.target.name] = event.target.value.startsWith("---")
      ? ""
      : event.target.value;
    this.SetDataCopy(dataCopy);
  }

  handleDateChange(date) {
    let dataCopy = this.state.data;
    dataCopy["birthday"] = date;
    this.SetDataCopy(dataCopy);
  }

  handleAvatarFileChange(acceptedFiles) {
    let files = [];
    let dataCopy = this.state.data;
    acceptedFiles.map((file) => files.push(file.name));
    dataCopy["avatarFile"] = files;
    this.SetDataCopy(dataCopy);
  }

  handleColorChange(color) {
    let dataCopy = this.state.data;
    dataCopy["colorSelected"] = color?.map((c) => c.label) || [];
    this.SetDataCopy(dataCopy);
  }

  handleMultiSelectChange(event) {
    let dataCopy = this.state.data;
    let value = event.target.value;
    if (dataCopy.multiSelect.includes(value)) {
      dataCopy.multiSelect = dataCopy.multiSelect.filter(
        (copy) => copy !== value
      );
    } else {
      dataCopy.multiSelect.push(event.target.value);
    }
    this.SetDataCopy(dataCopy);
  }

  handleSubmit(event) {
    if (this.validator.allValid()) {
      alert("You submitted the form and stuff!");
    } else {
      this.validator.showMessages();
      window.scrollTo(0, 200);
      alert("Please fill the form correctly!");
    }
    event.preventDefault();
  }

  handleCheckboxChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    let stateCopy = this.state.data[name];
    let newStateCopy = stateCopy.includes(value)
      ? stateCopy.filter((sValue) => sValue !== value)
      : stateCopy.concat(value);
    this.setState({
      data: { ...this.state.data, ...{ [name]: newStateCopy } },
    });
  }

  render() {
    return (
      <div className="grid grid-cols-2 divide-x divide-gray-400">
        <div>
          <h1 className="font-bold text-center text-4xl">Input</h1>
          <form
            className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4"
            onSubmit={this.handleSubmit}
          >
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Name:
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              name="name"
              type="text"
              placeholder="Your Name"
              value={this.state.data.name}
              onChange={this.handleStringInputChange}
            />
            {this.validator.message(
              "name",
              this.state.data.name,
              "required|alpha",
              { className: "text-red-600" }
            )}
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              E-Mail:
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              name="email"
              type="email"
              placeholder="jan@example.com"
              value={this.state.data.email}
              onChange={this.handleStringInputChange}
            />
            {this.validator.message(
              "email",
              this.state.data.email,
              "required|email",
              { className: "text-red-600" }
            )}
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Password:
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              name="password"
              type="password"
              placeholder="******************"
              value={this.state.data.password}
              onChange={this.handleStringInputChange}
            />
            {this.validator.message(
              "password",
              this.state.data.password,
              "required",
              { className: "text-red-600" }
            )}
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Avatar:
            </label>
            <label className="block shadow text-center appearance-none border rounded mt-2  w-40 py-2 px-3 cursor-pointer">
              <Dropzone
                onDrop={(acceptedFiles) => {
                  this.handleAvatarFileChange(acceptedFiles);
                }}
              >
                {({ getRootProps, getInputProps }) => (
                  <section>
                    <div {...getRootProps()}>
                      <input {...getInputProps()} />
                      <p>Browse...</p>
                    </div>
                  </section>
                )}
              </Dropzone>
            </label>
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Selected:{" "}
              <ol className="list-decimal">
                {this.state.data.avatarFile.map((file) => {
                  return <li className="text-red-600">{file}</li>;
                })}
              </ol>
            </label>
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Bio:
            </label>
            <textarea
              className="form-textarea mt-1 shadow appearance-none border rounded w-full break-words py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              rows="3"
              name="bio"
              type="text"
              value={this.state.data.bio}
              onChange={this.handleStringInputChange}
              placeholder="Tell us your story..."
              onBlur={() => this.validator.showMessageFor("bio")}
            ></textarea>
            {this.validator.message(
              "bio",
              this.state.data.bio,
              "required|min:20|max:120",
              { className: "text-red-600" }
            )}
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Birthday:
            </label>
            <DatePicker
              className="text-center shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              selected={this.state.data.birthday}
              onChange={this.handleDateChange}
              name="birthday"
              dateFormat="dd/MM/yyyy"
            />
            {this.validator.message(
              "date",
              this.state.data.birthday,
              "required",
              { className: "text-red-600" }
            )}
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Fruit :
            </label>
            {this.state.options.fruitsOptions.map((fruit, index) => {
              const checked = this.state.data.fruits.includes(
                fruit.id.toString()
              );
              return (
                <label className="block inline-flex items-center text-gray-700 mt-2  text-sm font-bold mb-2">
                  <input
                    name="fruits"
                    className="ml-2 mr-1 leading-tight"
                    id={fruit.value}
                    type="checkbox"
                    checked={checked}
                    value={fruit.id}
                    onChange={this.handleCheckboxChange}
                  />
                  {fruit.value}
                </label>
              );
            })}
            {this.validator.message(
              "fruits",
              this.state.options.fruitsOptions,
              "required",
              { className: "text-red-600" }
            )}
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Number of Friends:
            </label>
            <input
              name="numberOfFriends"
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              type="number"
              value={this.state.data.numberOfFriends}
              onChange={this.handleStringInputChange}
            />
            <br /> <br />
            <label className="text-gray-700 text-sm font-bold mb-2">
              Gender:
            </label>
            <label className="text-gray-700 ml-2 text-sm mb-2">
              Male
              <input
                name="gender"
                className="form-radio ml-2"
                value="Male"
                checked={this.state.data.gender === "Male"}
                onChange={this.handleStringInputChange}
                type="radio"
              />
            </label>
            <label className="text-gray-700 ml-2 text-sm mb-2">
              Female
              <input
                name="gender"
                className="form-radio ml-2"
                value="Female"
                checked={this.state.data.gender === "Female"}
                onChange={this.handleStringInputChange}
                type="radio"
              />
            </label>
            {this.validator.message(
              "gender",
              this.state.data.gender,
              "required",
              { className: "text-red-600" }
            )}
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Requested Limit:
            </label>
            <select
              name="requestLimit"
              value={this.state.data.requestLimit}
              onChange={this.handleStringInputChange}
              className="form-select mt-1 bg-white border rounded w-full py-2 px-3 text-gray-700 leading-tight"
            >
              {this.state.options.requestLimitOptions.map(
                (requestLimitOption, index) => {
                  return <option>{requestLimitOption.value}</option>;
                }
              )}
            </select>
            <br />
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Range (between 0 and 50):
            </label>
            <input
              type="range"
              className="ml-2 mt-2"
              id="R"
              name="range"
              min="0"
              max="50"
              value={this.state.data.range}
              onChange={this.handleStringInputChange}
            />
            <br />
            <br />
            <label className="block">
              <span className="block text-gray-700 text-sm font-bold mt-2 mb-2">
                Multiselect
              </span>
              <select
                name="multiSelect"
                value={this.state.data.multiSelect}
                onChange={this.handleMultiSelectChange}
                className="form-multiselect block bg-white border rounded w-full py-2 px-3 text-gray-700 leading-tight mt-1"
                multiple
              >
                {this.state.options.multiSelectOptions.map(
                  (multiSelectOption, index) => {
                    return <option>{multiSelectOption.value}</option>;
                  }
                )}
              </select>
            </label>
            <br />
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              List of names :
            </label>
            {this.state.options.namesOptions.map((name, index) => {
              const checked = this.state.data.names.includes(
                name.id.toString()
              );
              return (
                <label className="block items-center text-gray-700 mt-2  text-sm font-bold mb-2">
                  <input
                    name="names"
                    className="ml-2 mr-1 leading-tight"
                    id={name.value}
                    type="checkbox"
                    checked={checked}
                    value={name.id}
                    onChange={this.handleCheckboxChange}
                  />
                  {name.value}
                </label>
              );
            })}
            <br />
            <label className="block text-gray-700 text-sm font-bold mt-2 mb-2">
              Select a color :
            </label>
            <Select
              isMulti
              name="colors"
              options={this.state.options.colorOptions}
              className="basic-multi-select"
              classNamePrefix="select"
              onChange={this.handleColorChange}
            />
            <br />
            <br />
            <input
              type="submit"
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              value="Submit"
            />
          </form>
        </div>
        <RightPane fdata={this.state} />
      </div>
    );
  }
}

export default DetailedForm;
