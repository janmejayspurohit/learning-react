import React from "react";
import FormProvider from "./FormProvider";
import RightPane2 from "./RightPane2";
import LeftPane from "./LeftPane";
import SimpleReactValidator from "simple-react-validator";
// import Select from "react-select";
// import DatePicker from "react-datepicker";
// import "react-datepicker/dist/react-datepicker.css";
// import Dropzone from "react-dropzone";

class FinalForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validator: new SimpleReactValidator({ autoForceUpdate: this }),
      data: {
        name: "",
        email: "",
        password: "",
        avatarFile: [],
        birthday: new Date(),
        bio: "",
        numberOfFriends: "",
        gender: "",
        requestLimit: "",
        range: "",
        colorSelected: [],
        multiSelect: [],
        fruits: [],
        names: [],
      },
      options: {
        colorOptions: [
          { value: "ocean", label: "Ocean", color: "#00B8D9" },
          { value: "blue", label: "Blue", color: "#0052CC" },
          { value: "purple", label: "Purple", color: "#5243AA" },
          { value: "red", label: "Red", color: "#FF5630" },
          { value: "orange", label: "Orange", color: "#FF8B00" },
          { value: "yellow", label: "Yellow", color: "#FFC400" },
          { value: "green", label: "Green", color: "#36B37E" },
          { value: "forest", label: "Forest", color: "#00875A" },
          { value: "slate", label: "Slate", color: "#253858" },
          { value: "silver", label: "Silver", color: "#666666" },
        ],
        multiSelectOptions: [
          { id: 1, value: "Option 1" },
          { id: 2, value: "Option 2" },
          { id: 3, value: "Option 3" },
          { id: 4, value: "Option 4" },
          { id: 5, value: "Option 5" },
          { id: 6, value: "Option 6" },
          { id: 7, value: "Option 7" },
          { id: 8, value: "Option 8" },
          { id: 9, value: "Option 9" },
          { id: 10, value: "Option 10" },
        ],
        requestLimitOptions: [
          { id: 1, value: "---Select a value---" },
          { id: 2, value: "$1000" },
          { id: 3, value: "$2000" },
          { id: 4, value: "$3000" },
          { id: 5, value: "$4000" },
          { id: 5, value: "$5000" },
        ],
        fruitsOptions: [
          { id: 1, value: "Apple" },
          { id: 2, value: "Banana" },
          { id: 3, value: "Cherry" },
          { id: 4, value: "Coconut" },
          { id: 5, value: "Grape" },
          { id: 6, value: "Lime" },
          { id: 7, value: "Mango" },
          { id: 8, value: "Orange" },
          { id: 9, value: "Pear" },
          { id: 10, value: "Pineapple " },
        ],
        namesOptions: [
          { id: 1, value: "Name1" },
          { id: 2, value: "Name2" },
          { id: 3, value: "Name3" },
          { id: 4, value: "Name4" },
          { id: 5, value: "Name5" },
          { id: 6, value: "Name6" },
          { id: 7, value: "Name7" },
          { id: 8, value: "Name8" },
          { id: 9, value: "Name9" },
          { id: 10, value: "Name10" },
        ],
      },
    };
  }

  SetDataCopy = (dataCopy) => {
    this.setState({
      data: dataCopy,
    });
  };

  handleStringInputChange = (event) => {
    let dataCopy = this.state.data;
    dataCopy[event.target.name] = event.target.value.startsWith("---")
      ? ""
      : event.target.value;
    this.SetDataCopy(dataCopy);
  };

  handleDateChange = (date) => {
    let dataCopy = this.state.data;
    dataCopy["birthday"] = date;
    this.SetDataCopy(dataCopy);
  };

  handleAvatarFileChange = (acceptedFiles) => {
    let files = [];
    let dataCopy = this.state.data;
    acceptedFiles.map((file) => files.push(file.name));
    dataCopy["avatarFile"] = files;
    this.SetDataCopy(dataCopy);
  };

  handleColorChange = (color) => {
    let dataCopy = this.state.data;
    dataCopy["colorSelected"] = color?.map((c) => c.label) || [];
    this.SetDataCopy(dataCopy);
  };

  handleMultiSelectChange = (event) => {
    let dataCopy = this.state.data;
    let value = event.target.value;
    if (dataCopy.multiSelect.includes(value)) {
      dataCopy.multiSelect = dataCopy.multiSelect.filter(
        (copy) => copy !== value
      );
    } else {
      dataCopy.multiSelect.push(event.target.value);
    }
    this.SetDataCopy(dataCopy);
  };

  handleSubmit = (event) => {
    event.preventDefault();
    let errorObject = this.state.validator.errorMessages;

    let offset = () => {
      for (const key in errorObject) {
        if (errorObject[key]) {
          return document.getElementsByName(key.toString())[0].offsetTop;
        }
      }
    };

    let offset_value = offset();

    if (this.state.validator.allValid()) {
      alert("You submitted the form and stuff!");
    } else {
      this.state.validator.showMessages();
      window.scrollTo(0, parseInt(offset_value) - 40);
      alert("Please fill the form correctly!");
    }
  };

  handleCheckboxChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    let stateCopy = this.state.data[name];
    let newStateCopy = stateCopy.includes(value)
      ? stateCopy.filter((sValue) => sValue !== value)
      : stateCopy.concat(value);
    this.setState({
      data: { ...this.state.data, ...{ [name]: newStateCopy } },
    });
  };

  render() {
    const contextData = {
      ...this.state,
      handleStringInputChange: this.handleStringInputChange,
      handleDateChange: this.handleDateChange,
      handleColorChange: this.handleColorChange,
      handleMultiSelectChange: this.handleMultiSelectChange,
      handleSubmit: this.handleSubmit,
      handleCheckboxChange: this.handleCheckboxChange,
      handleAvatarFileChange: this.handleAvatarFileChange,
    };

    return (
      <FormProvider.Provider value={contextData}>
        <div className="grid grid-cols-2 divide-x divide-gray-400">
          <LeftPane />
          <RightPane2 />
        </div>
      </FormProvider.Provider>
    );
  }
}

export default FinalForm;
