import React from "react";
// import './App.css';
// import Hook from'./component/hook.js'
import Header from "./component/header.js";
import Footer from "./component/footer.js";
import Lists from "./component/Lists.js";
import Forms from "./component/forms.js";
import DetailedForm from "./component/DetailedForm.js";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Home from "./Views/Home";
import About from "./Views/About";
import Tesark from "./Views/Tesark";
import Product from "./Views/Product";
import FinalForm from "./component/FinalForm.js";

function App() {
  const numbers = [1, 2, 3, 4, 5];

  return (
    <div>
      <Router>
        <Header />
        <div className="p-3">
          <Switch>
            <Route exact path="/">
              <Home />
              <Lists numbers={numbers} />
              <Forms />
              <Link to="/form" className="text-blue-500 py-3 border-t border-b">
                <button className="bg-blue-300 hover:bg-blue-400 text-black font-bold py-2 px-4 rounded">
                  Forms➜
                </button>
              </Link>
              <Link
                to="/new_form"
                className="text-blue-500 py-3 border-t border-b"
              >
                <button className="bg-blue-300 hover:bg-blue-400 text-black font-bold py-2 px-4 rounded">
                  New Form➜
                </button>
              </Link>
            </Route>
            <Route path="/about">
              <About />
            </Route>
            <Route path="/tesark">
              <Tesark />
            </Route>
            <Route path="/form">
              <Link to="/" className="text-blue-500 py-3 border-t border-b">
                <button className="bg-blue-300 hover:bg-blue-400 text-black font-bold py-2 px-4 rounded">
                  ◀Home
                </button>
              </Link>
              <DetailedForm />
            </Route>
            <Route path="/new_form">
              <Link to="/" className="text-blue-500 py-3 border-t border-b">
                <button className="bg-blue-300 hover:bg-blue-400 text-black font-bold py-2 px-4 rounded">
                  ◀Home
                </button>
              </Link>
              <FinalForm />
            </Route>
            <Route path="/product/:id">
              <Product />
            </Route>
          </Switch>
        </div>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
