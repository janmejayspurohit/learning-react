import React, { useState, useEffect } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
 
function Product() {
	const { id } = useParams();
	const url = id;
	const [product, setProduct] = useState(null);
	let content =
		"https://tesark-wp.s3.amazonaws.com/Media/Desktop/Desktop-Lines%281920x1080%29.png";

	useEffect(() => {
		axios.get(url).then((response) => {
			setProduct(response.data);
		});
	}, [url]);

	if (product) {
		content = product;
	}

	return <img src={content} alt="TESARK"></img>;
}

export default Product;
