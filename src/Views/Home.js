import React from "react";
import HelloWorld from "../component/hw.js";

function Home() {
	return (
		<div>
			<h1 className="font-bold text-2xl">Home Page</h1>
			<HelloWorld name="Jan!!" />
		</div>
	);
}

export default Home;
